class Person {
  constructor(_name, _age, _salary, _gender) {
    this.name = _name;
    this.age = _age;
    this.salary = _salary;
    this.gender = _gender;
  }

  static sort(array, field, order) {
    let Arr = [...array];
    let n = Arr.length;
    function swap(arr, i, j) {
      let temp = arr[i];
      arr[i] = arr[j];
      arr[j] = temp;
    }
    function compare(order, arr, j, field, pivot) {
      if (order == "inc") return arr[j][field] < pivot[field];
      else if (order == "des") return arr[j][field] > pivot[field];
    }
    function partition(arr, low, high, field, order) {
      let pivot = arr[high];
      let i = low - 1;
      for (let j = low; j <= high - 1; j++) {
        if (compare(order, arr, j, field, pivot)) {
          i++;
          swap(arr, i, j);
        }
      }
      swap(arr, i + 1, high);
      return i + 1;
    }

    function quickSort(arr, low, high, field, order) {
      if (low < high) {
        let pi = partition(arr, low, high, field, order);
        quickSort(arr, low, pi - 1, field, order);
        quickSort(arr, pi + 1, high, field, order);
      }
    }
    quickSort(Arr, 0, n - 1, field, order);
    console.log(Arr);
  }
}
let array = [
  new Person("c", 21, 13, "Male"),
  new Person("a", 22, 14, "Male"),
  new Person("b", 23, 15, "Male"),
  new Person("d", 24, 16, "Male"),
  new Person("e", 25, 17, "Male"),
];
console.log(array);
Person.sort(array, "salary", "inc");
